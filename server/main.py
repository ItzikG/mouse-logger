import pymongo as pymongo
from flask import Flask, request
from flask_cors import CORS

app = Flask(__name__)
CORS(app)

client = pymongo.MongoClient(
    "mongodb+srv://zion:DOXVm9b32Bapf3txN@cluster0.abjsp.mongodb.net/<dbname>?retryWrites=true&w=majority")

db = client['zion']
points_col = db['points']


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/point', methods=['POST'])
def point():
    # print('points' in request.json)
    if 'points' in request.json and 'session' in request.json:
        points_col.insert_one(request.json)

    return 'OK'


if __name__ == "__main__":
    app.run(debug=True)
