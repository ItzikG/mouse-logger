import {Component, OnInit, ViewChild} from '@angular/core';
import {Observable, fromEvent} from 'rxjs';
import {ApiService} from '../api.service';
import {Router, ActivatedRoute, ParamMap, RoutesRecognized} from '@angular/router';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

import uuid from 'uuid';

@Component({
  selector: 'app-mouse',
  templateUrl: './mouse.component.html',
  styleUrls: ['./mouse.component.scss']
})
export class MouseComponent implements OnInit {
  points = [];
  mouseX = 0.0;
  mouseY = 0.0;
  pointTime = Date.now();
  user = null;
  private recordInterval = 100; // millis
  sendToServerEvery = 300; // How many points to gather till we send to server
  id = uuid.v4();
  videoId = null;
  @ViewChild('player') player: any;

  constructor(private route: ActivatedRoute, private router: Router, private api: ApiService, public sanitizer: DomSanitizer) {
    // Get user id from params
    this.router.events.subscribe(val => {
      if (val instanceof RoutesRecognized) {
        const params = val.state.root.firstChild.params;
        if ('user' in params) {
          this.user = params.user;
          console.log('User id:', this.user);
        }
      }
    });
    // Get video Id from params
    this.route.queryParams.subscribe(params => {
      if ('v' in params) {
        this.videoId =  params.v;
        console.log('Video id:', this.videoId);
      }

    });
  }

  ngOnInit(): void {
    console.log('session id', this.id);

    this.points = [];
    console.log('points', this.points);

    fromEvent(document.body, 'mousemove').subscribe((e: MouseEvent) => {
      this.mouseX = e.pageX;
      this.mouseY = e.pageY;
      this.pointTime = Date.now();
    });

    // fromEvent(document.body, 'mousemove').subscribe((e: MouseEvent) => { console.log('moved'); });

    setInterval(() => {
        const self = this;

        self.points.push({
          x: self.mouseX,
          y: self.mouseY,
          time: self.pointTime

        });
        // Send to server
        if (self.points.length >= self.sendToServerEvery) {
          self.api.sendPoints(self.id, self.points, self.user);
          self.points = [];
        }

      },
      this.recordInterval);

  }
  // Autoplay
  onReady(event) {
    this.player.mute();
    this.player.playVideo();
  }

  // Loop
  onStateChange(event) {
    if (event.data === 0) {
      this.player.playVideo();
    }
  }


}
