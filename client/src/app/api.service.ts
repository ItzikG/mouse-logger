import {Injectable} from '@angular/core';
import {environment} from './../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }


  sendPoints(session, points: any[], user: string) {
    this.http.post(environment.serverPath + '/point', {points, session, user, time: new Date().toISOString()}).subscribe(value => console.log(value), error => console.error(error));
  }
}
