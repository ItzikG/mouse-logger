import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {MouseComponent} from './mouse/mouse.component';
import {HttpClientModule} from '@angular/common/http';
import {SanitizeUrlPipe} from './mouse/SanitizeUrlPipe';
import { Page404Component } from './page404/page404.component';
import {YouTubePlayerModule} from '@angular/youtube-player';

@NgModule({
  declarations: [
    AppComponent,
    MouseComponent,
    SanitizeUrlPipe,
    Page404Component
  ],
  imports: [
    BrowserModule,
    YouTubePlayerModule,
    AppRoutingModule, HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
