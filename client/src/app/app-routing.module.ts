import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MouseComponent} from './mouse/mouse.component';


const routes: Routes = [
  { path: ':user', component:  MouseComponent},
   { path: '404', component:  MouseComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
